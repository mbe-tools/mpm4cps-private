\chapter{Introduction}
\label{ch:introduction}

\authors{Bedir Tekinerdogan, Dominique Blouin, Hans Vangheluwe, Miguel Goulão, Paulo Carreira, Vasco Amaral}

%Bedir Tekinerdogan, Wageningen University, The Netherlands \newline
%Dominique Blouin, Telecom Paris, Institut Polytechnique de Paris, France \newline
%Hans Vangheluwe, University of Antwerp, Belgium \newline
%Miguel Goulão, NOVA-LINCS and Universidade NOVA de Lisboa, Portugal \newline
%Paulo Carreira, Instituto Superior Técnico, Portugal \newline
%Vasco Amaral, NOVA-LINCS and Universidade NOVA de Lisboa, Portugal \newline
%%
%\author[1]{Bedir Tekinerdogan}
%%\author[2]{Dominique Blouin}
%%\author[3]{Hans Vangheluwe}
%%\author[4]{Miguel Goulão}
%%\author[5]{Paulo Carreira}
%%\author[6]{Vasco Amaral}
%\address[1]{Wageningen University, The Netherlands}
%%\address[2]{Telecom Paris, Institut Polytechnique de Paris, France}
%%\address[3]{University of Antwerp, Belgium}
%%\address[4]{Universidade Nova de Lisboa, Portugal}
%%\address[5]{Instituto Superior Técnico, Portugal}
%%\address[6]{Universidade Nova de Lisboa, Portugal}
%

\section{Objectives}

Truly complex, multidisciplinary, engineered systems, known as Cyber-Physical Systems (CPS), are emerging in today's reality. Those integrate physical, software, and network aspects in a sometimes adverse physical environment. We can find examples of CPS in autonomous cars, industrial control systems, robotics systems, medical monitoring, and automatic pilot avionics, to name a few. The cover of the book, for instance, shows another example of a complex CPS, the Maltese Falcon sailing yacht, one of the world's most complex and largest yachts. The act of sailing consists of employing the wind, acting on sails, propelling the craft on the surface of the water in the most different environment scenarios (the Sea conditions can be somewhat unpredictable). Operation of these crafts, with speed and fuel-saving concerns, is traditionally done by skilled skippers and can be appreciated as a sport. Here, like in other sports such as Formula-1, automation stepped into the design, simulation to support the decision and optimize, and operation in both stationary and unsteady conditions. Here, besides the several degrees of freedom of motion, a complex dynamic of forces must be controlled by balancing hydrodynamics, aerodynamics, buoyant and gravitational forces.  

As Maltese Falcon, to build such boats is a complicated engineering endeavor with challenging technological and physical constraints (with impact in the used materials, communications, etc). Maltese Falcon has 88 meters long (289 feet) and can be operated by a single person. To do that, she counts with a wide plethora of actuating devices, including freestanding rotating masts. Also, to support a complex control logic, there is a complex set of sensors with advanced technology such as fiber optical strain net into the spars to analyze real-time loads under sail. The yacht's sophisticated computer software can automatically detect parameters such as wind speed and display critical data to the operator. This autonomy property is characteristic of many CPSs.

Despite the increased adoption and impact of CPSs (where Maltese Falcon is an example), no unifying theory nor systematic design methods, techniques, and tools exist for such systems. Individual (mechanical, electrical, network, or software) engineering disciplines only offer partial solutions. Multi-Paradigm Modeling (MPM) proposes to model every part and aspect of a system explicitly, at the most appropriate level(s) of abstraction, using the most appropriate modeling formalism(s). Modeling languages engineering, including model transformation, and the study of their semantics, are used to realize MPM. MPM is seen as an effective answer to the challenges of designing CPS. 

Modeling and analysis are crucial activities in the development of Cyber-Physical Systems. Moreover, the inherent cross-disciplinary nature of CPS requires distinct modeling techniques related to different disciplines to be employed. At the same time, to enable communication between all specialties, common background knowledge is needed. 

Anyone starting in the field of CPS will be faced with the need for literature with solid foundations of modeling CPS and with a comprehensive introduction to the distinct existing techniques with clear reasoning on their advantages and limitations. Indeed, although most of these techniques are already used as a matter of common practice in specific disciplines, the knowledge of their fundamentals and application is typically far away from practitioners of another area. The net result is the tendency for CPS practitioners to use the technique that they are most comfortable with, disregarding the technique that would be the most adequate for the problem and modeling goal.

This book is the result of cooperation made possible by the COST Action IC1404 "Multi-Paradigm Modeling for Cyber-Physical Systems" (MPM4CPS), which allowed researchers, institutions and companies from 32 countries to collaborate over four years.

The goal of this book is to serve as a showcase of the research outcomes of the different workgroups of the MPM4CPS network. As such, the book is expected to cover the results on the foundations, formalisms, tools, and educational resources produced within the MPM4CPS network (e.g., case studies made available by the MPM4CPS network). The text will focus on state-of-the-art research and practice knowledge.

The book includes both chapters that discuss experiences from the industry and papers that are more research-oriented. Practitioners will benefit from the book by identifying the critical problems, the solution approaches, and the tools that have been developed or are necessary for model management and analytics. Researchers will benefit from the book by identifying the underlying theory and background, the current research topics, the related challenges, and the research directions for the model management and analytics. The book will also help graduate students, researchers, and practitioners to get acquainted with recent research outcomes of the MPM4CPS network.

\section{Outline of the book}

The book consists of three basic parts: an ontological framework for MPM4CPS; methods and tools; and, finally, case studies. 
Part I provides the ontological framework for MPM4CPS and includes chapter 2 to chapter 5. The ontology framework is decomposed into four sub-parts: a shared ontology to capture concepts that are needed by the other ontologies but that do not pertain to their domains, an ontology for CPS, an ontology for MPM and an integrated ontology for MPM4CPS. For deriving the ontologies, a thorough domain analysis process has been carried out that has focused on key selected primary studies on MPM4CPS. 
Part II describes the methods and tools that are used to develop and analyze CPSs. Chapter 6 to chapter 9 cover these topics.
Part III of the book considers case studies which are addressed in chapter 10 and chapter 11. 

\subsection{ Part I - Ontological Framework}
Chapter~\ref{ch:foundations:introduction} introduces the modeling approach and tools that have been chosen to define the ontology, to both motivate and clarify the methods and the actual directions that led the research effort. It also provides a description of the shared ontology whose concepts are used to frame in a more general context some of the more specific notions of the other ontologies. Then, the chapter presents a brief description of the examples we adopted as references to start exploring the field, to glance at our vision of the domain and to guide readers through our exploration path. 

The ontology for CPS is described in chapter~\ref{ch:foundations:CPS-Ontology}. Hereby, a feature modeling approach is adopted that explicitly models the common and variant features of a CPS. Each feature of the resulting feature model is described in detail. The resulting feature model shows the configuration space for developing CPSs. The two case studies on CPS that were introduced in chapter~\ref{ch:foundations:introduction} are used to derive the concrete CPS configurations.  

Chapter~\ref{ch:foundations:MPM-Ontology} presents the ontology for MPM specified using the Web Ontology Language (OWL). The chapter first presents a thorough state of the art on MPM's core notions, multi-formalism and model management approaches, languages, and tools, which is an essential component to support MPM. Model management approaches are characterized according to their modularity and incremental execution properties as required to scale for the large complex systems we face today. Subsequently, an overview of the MPM ontology is developed, including the main classes and properties of the ontology. Usage of the MPM ontology is illustrated for the two case studies introduced in chapter~\ref{ch:foundations:introduction}.

Chapter~\ref{ch:foundations:MPM4CPS-Ontology} integrates the results of the previous chapters by presenting an integrated ontology for MPM4CPS. Hereby, the chapter also elaborates on and integrates the \emph{Shared}, \emph{CPS}, and \emph{MPM} ontologies by providing cross-cutting concepts between these domains. It formalizes notions such as model-based development processes, their employed viewpoints supported by megamodel fragments and the CPS parts under development covered by these viewpoints. It finally introduces some ongoing work at the heart of MPM4CPS on the formalization of modeling paradigm notions in the more general context of engineering paradigms.

\subsection{ Part II - Methods and Tools}
Chapter 6 presents the two-hemisphere model-driven (2HMD) approach for enabling the composition of CPSs. The approach assumes modeling and the use of procedural and conceptual knowledge on an equal and interrelated basis. This differentiates the 2HMD approach from pure procedural, purely conceptual, and object-oriented approaches. This approach may be applied in the context of modeling of a particular business domain as well as in the context of modeling the knowledge about the domain. Cyber-physical systems are heterogeneous systems that require a multi-disciplinary approach for their modeling. Modeling of cyber-physical systems by the 2HMD approach gives an opportunity to compose and analyze system components to be provided transparently and components actually provided, thus identifying and filling the gap between desirable and actual system content.

In chapter 7, the authors illustrate how a co-simulation technology can be used to gradually increase the details in a collaborative model (co-model) following a discrete event first" (DE-first) methodology. In this approach, initial abstract models are produced using a discrete event (DE) formalism (in this case, VDM) to identify the proper communication interfaces and interaction protocols among different models. These are gradually replaced by more detailed models using appropriate formalisms, for example, continuous-time (CT) models of physical phenomena.

Chapter 8 presents an agent-based CPS development approach using a domain-specific modeling language, SEA ML++. The paper elaborates on intelligent agents, which are software components that can work autonomously and proactively to solve the problems collaboratively. Agents can behave in a cooperative manner and collaborate with other agents constituting systems called Multi-agent Systems (MAS). Intelligent software agents and MASs can be used in the modeling and development of CPSs. In this chapter, the authors discuss how SEA ML++ is used for the design and implementation of agent-based CPSs. An MDE methodology is introduced in which SEA ML++ can be used to design agent-based CPS and implement these systems on various agent execution platforms. As evaluating case study, the development of a multi-agent garbage collection CPS is taken into consideration. The conducted study demonstrates how this CPS can be designed according to the various viewpoints of SEA ML++ and then implemented on JASON agent execution platform.

Chapter 9 focuses on hybrid systems modeling, which is essential for CPS. The expressiveness for hybrid systems modeling allows for the definition of highly complex systems that merge discrete state-based transitions systems with continuous value-evolutions for variables. That way, cyber-physical systems can be modeled in all their intricacies. However, this expressive power comes at a downside of complex models and undecidable verification problems even for small systems. In this chapter, the authors present CREST, a novel modeling language for the definition of hybrid systems. CREST merges features from various formalisms and languages such as hybrid automata, data flow programming, and internal DSL designs to create a simple yet powerful language for modeling resource flows within small-scale CPS such as automated gardening applications and smart homes. The language provides an easy-to-learn graphical interface and is supported by a Python-based tool implementation that allows the efficient modeling, simulation, and verification of CPS models.

\subsection{ Part III - Case Studies}
Chapter 10 describes the design and development of an IoT and WSN based CPS using MPM Approach for a Smart Fire Detection Case Study. The system is developed using the Internet of Things (IoT) components and Wireless Sensor Network (WSN) elements. The proposed system is composed of different hardware parts, software elements, computing components, and communication technologies, resulting in a complex system considering both its structure and behavior. The chapter elaborates on different phases of the development process, including requirement analysis, design, modeling and simulation, and implementation. To present the MPM approach during these phases, the Formalism Transformation Graph and Process Model (FTG+PM) is utilized, and all the involved artifacts and model transformations are described. This helps to provide the data flow and the control flow of the system development in the PM. Further, the analysis of the FTG shows the possible improvements for the system by finding the critical manual transformation to be (semi-)automated.

Chapter 11 presents the development of industry-oriented cross-domain study programs in CPSs for Belarusian and Ukrainian Universities. The paper aligns with the targets of the European COST Action MPM4CPS project, which also considered the dissemination of the results in the educational context. The chapter presents the process and results for creating the base for a European Master and Ph.D. program in MPM4CPS involving European Leading Universities. Further, it elaborates on setting up the respective discipline road-map facing the challenge to develop a mutually recognized cross-domain expertise based study program in CPS. One of the challenges in the development of study programs is bridging the gap between industry needs and educational output, in terms of training the prospective researchers and engineers in the CPS field. The success of MPM4CPS encouraged EU partners to apply knowledge and methods, developed by the COST Action at an ERASMUS+ project, to validate in practice its viability aiming to develop the industry-focused curricula at partners' universities of Belarus and Ukraine. The chapter discusses how the COST team efforts towards an analysis of tendencies, industry needs, and acquiring best education practices have been applied by the ERASMUS+ team to create industry-focused cross-domain study programs in CPS for the partners' universities of Belarus and Ukraine.

\section{Acknowledgments}

This book was supported by the COST Action IC1404 Multi-Paradigm Modeling for Cyber-Physical Systems (MPM4CPS), COST is supported by the EU Framework Programme Horizon 2020.

INESC-ID authors were supported by national funds through FCT (Fundação para a Ciência e a Tecnologia) under contract UID/CEC/50021/2019. Also, NOVA authors were supported by NOVA LINCS Research Laboratory y (Ref.
UID/CEC/04516/2019) and bilateral project Portugal-Germany, “Modelação de Sistemas Sócio Ciberfísicos”, Proc. 441.00 DAAD.
%% Insert all the institutional support? in the case of Portugal, it is mandatory.

Telecom Paris authors were partially supported by the US Army Research, Development and Engineering Command (RDECOM).


Finally, We would like to thank all the authors and contributors of the chapters.



